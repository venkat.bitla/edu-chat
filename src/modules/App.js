import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import NavBar from '../components/NavBar';
import HomePage from '../pages/HomePage';
import DashBoard from '../pages/DashBoard';

export default class App extends Component {

  render() {
    return (
      <div>
        <header>
          <NavBar/>
        </header>
        <main>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path={this.props.match.url + 'dashboard'} component={DashBoard} />
          </Switch>
        </main>
      </div>
    );
  }
}